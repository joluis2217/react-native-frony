import {AppRegistry} from 'react-native';
import ClientMaintenanceTemplate from './src/screens/ClientMaintenancePage';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ClientMaintenanceTemplate);
