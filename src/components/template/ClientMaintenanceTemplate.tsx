import React,{  useState } from 'react';
import { View, StyleSheet } from 'react-native';
import HeaderOrganism from '../organisms/Client/HeaderClient';
import ClientMaintenanceOrganism from '../organisms/Client/ClientsSection';
import ButtonAtom from '../atoms/Client/ButtonAtom';
import AddClientFormModal from '../../forms/AddClientFormModal';
import UpdateClientFormModal from '../../forms/UpdateClientFormModal';
import { ClientData } from '../../models/Client';
import  CustomAlert from '../../utils/utils/alert';
import  CustomAlertProp from '../../utils/utils/alertProp';

interface ClientMaintenanceTemplateProps {
  title: string;
  iconSource?: any;
  clients: ClientData[];
  onClientPress: (client: ClientData) => void;
  onClientLongPress:(client:ClientData)=>void;
  onAddClient: (clientData: ClientData) => void;
  selectedClient: ClientData | null;
  isUpdateFormVisible: boolean;
  setIsUpdateFormVisible: (isVisible: boolean) => void;
  onUpdateClient: (clientData: ClientData) => void;
  children?: React.ReactNode;
  IsAlertVisible: boolean;
  SetIsAlertVisible: (isVisible: boolean) => void;
  alertContent:string;
  IsAlertVisibleProp: boolean;
  SetIsAlertVisibleProp: (isVisible: boolean) => void;
  alertContentProp:string;
  onConfirmProp:()=>void
}

const ClientMaintenanceTemplate: React.FC<ClientMaintenanceTemplateProps> = ({
  title,
  iconSource,
  clients,
  onClientPress,
  onClientLongPress,
  onAddClient,
  selectedClient,
  isUpdateFormVisible,
  setIsUpdateFormVisible,
  onUpdateClient,
  IsAlertVisible,
  SetIsAlertVisible,
  alertContent,
  IsAlertVisibleProp,
  SetIsAlertVisibleProp,
  alertContentProp,
  onConfirmProp
}) => {

  const [isPopupVisible, setIsPopupVisible] = useState(false);

  const togglePopup = () => {
    setIsPopupVisible(!isPopupVisible);
  };

  const handleCloseUpdateForm = () => {
    setIsUpdateFormVisible(false);
  };
  
 const onhideAlert=() => {
    SetIsAlertVisible(false);
  };
  const onhideAlertProp=() => {
    SetIsAlertVisibleProp(false);
  };

  return (
    <View style={styles.container}>
      <HeaderOrganism title={title} iconSource={iconSource} />
      <ClientMaintenanceOrganism clients={clients} onClientPress={onClientPress}  onClientLongPress={onClientLongPress} />
      <View style={styles.floatingButtonContainer}>
        <ButtonAtom onPress={togglePopup} />
      </View>
    {isPopupVisible  && (
        <AddClientFormModal
          isVisible={isPopupVisible}
          onClose={togglePopup}
          onAddClient={onAddClient}
        />
      )}
    {isUpdateFormVisible && (
        <UpdateClientFormModal
          isVisible={isUpdateFormVisible}
          onClose={handleCloseUpdateForm}
          onUpdateClient={onUpdateClient}
          selectedClient={selectedClient}
        />
      )}
      {IsAlertVisible && (
    <CustomAlert isVisible={IsAlertVisible} content={alertContent} onClose={onhideAlert} />)}
  {IsAlertVisibleProp && (
   <CustomAlertProp isVisible={IsAlertVisibleProp} content={alertContentProp} onConfirm={onConfirmProp} onClose={onhideAlertProp} />)}

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  floatingButtonContainer: {
    position: 'absolute',
    bottom: 20,
    right: 20,
  },
});

export default ClientMaintenanceTemplate;
