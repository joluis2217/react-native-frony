import React from 'react';
import { View, StyleSheet } from 'react-native';
import ClientList from '../../molecules/Client/ClientList'; // Importa la molécula ClientList
import {ClientData} from '../../../models/Client';
const ClientSection: React.FC<{ clients: ClientData[]; onClientPress: (client: ClientData) => void;onClientLongPress: (client: ClientData) => void  }> = ({
  clients,
  onClientPress,
  onClientLongPress,
}) => {
  return (
    <View style={styles.container}>
      <ClientList clients={clients} onClientPress={onClientPress} onClientLongPress={onClientLongPress} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
});

export default ClientSection;