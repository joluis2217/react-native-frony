import React from 'react';
import { View, StyleSheet } from 'react-native';
import Header from '../../molecules/Client/Header';

const HeaderOrganism: React.FC<{
  title: string;
  iconSource?: any;
}> = ({ title, iconSource }) => {
  return (
    <View style={styles.headerContainer}>
      <Header title={title} iconSource={iconSource} />
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    
    backgroundColor: '#ffffff',
  },
});
export default HeaderOrganism;