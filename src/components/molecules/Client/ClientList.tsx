import React from 'react';
import { FlatList, View, StyleSheet } from 'react-native';
import ClientItem from '../../atoms/Client/ClientItem';
import { ClientData } from '../../../models/Client';

const ClientList: React.FC<{ clients: ClientData[]; onClientPress: (client: ClientData) => void, onClientLongPress: (client: ClientData) => void  }> = ({
  clients,
  onClientPress,
  onClientLongPress
}) => {
  const renderClientItem = ({ item }: { item: ClientData }) => {
    return (
      <View style={styles.itemContainer}>
        <ClientItem client={item} onPress={() => onClientPress(item)} onLongPress={()=>onClientLongPress(item)} />
      </View>
    );
  };

  return (
    <FlatList
      data={clients}
      renderItem={renderClientItem}
      keyExtractor={(item) => item.dni.toString()}
      contentContainerStyle={styles.listContainer}
    />
  );
};

const styles = StyleSheet.create({
  listContainer: {
    paddingHorizontal: 16,
    paddingTop: 8,
    paddingBottom: 16,
    backgroundColor: '#FFFFFF',
  },
  itemContainer: {
    marginVertical: 8,
    elevation: 2, 
    backgroundColor: 'white', 
    borderRadius: 8, 
  },
});

export default ClientList;
