import React from 'react';
import { Modal} from 'react-native';

interface PopupMoleculeProps {
  isVisible: boolean;
  onClose: () => void;
  children?: React.ReactNode; // Agregamos children aquí
}

const PopupMolecule: React.FC<PopupMoleculeProps> = ({ isVisible, children }) => {
  return (
    <Modal animationType="slide" transparent={true} visible={isVisible}>
          {children}
    </Modal>
  );
};

export default PopupMolecule;