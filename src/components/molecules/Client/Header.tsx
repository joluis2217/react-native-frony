import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';

const Header: React.FC<{ title: string; iconSource?: any }> = ({ title, iconSource }) => {
  return (
    <View style={styles.headerContainer}>
      <View style={styles.centeredContent}>
        {iconSource && <Image source={iconSource} style={styles.icon} />}
        <Text style={styles.title}>{title}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: '#ce0058',
    paddingVertical: 12,
    width: '100%', 
  },
  centeredContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 24,
    height: 24,
    marginRight: 8,
    tintColor: 'white',
  },
  title: {
    fontSize: 18,
    color: 'white',
    fontWeight: 'bold',
  },
});

export default Header;
