import React from 'react';
import { TouchableOpacity, Text, View, StyleSheet, Image } from 'react-native';
import { ClientData } from '../../../models/Client';

const ClientItem: React.FC<{ client: ClientData; onPress: () => void; onLongPress: () => void }> = ({ client, onPress, onLongPress }) => {
  return (
    <TouchableOpacity style={styles.item} onPress={onPress} onLongPress={onLongPress}>
     <View style={styles.imageContainer}>
     <Image source={require('../../../utils/images/clients.png')} style={styles.image} />
      </View>
    
      <View style={styles.clientData}>
        <Text style={styles.dni}>{client.dni}</Text>
        <Text style={styles.name}>{client.name}</Text>
        <Text style={styles.lastName}>{client.lastName}</Text>
        <Text style={styles.birthDate}>{client.birthDate}</Text>
        <Text style={styles.city}>{client.city}</Text>
      </View>
      <View style={styles.imageContainer}>
        <Image source={require('../../../utils/images/fish.png')} style={styles.image} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    borderRadius: 10,
    marginBottom: 10,
  },
  premiumItem: {
    backgroundColor: '#FFD700',
  },
  corporateItem: {
    backgroundColor: '#007BFF',
  },
  clientData: {
    flex: 1,
    flexDirection: 'column',
    marginLeft:20
  },
  dni: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  name: {
    fontSize: 14,
  },
  lastName: {
    fontSize: 14,
  },
  birthDate: {
    fontSize: 14,
  },
  city: {
    fontSize: 14,
  },
  imageContainer: {
    width: 40,
    alignItems: 'center',
  },
  image: {
    width: 40,
    height: 40,
  },
});

export default ClientItem;
