import React from 'react';
import { Image } from 'react-native';

const Icon: React.FC<{ source: any; width: number; height: number }> = ({ source, width, height }) => {
  return <Image source={source} style={{ width, height }} />;
};

export default Icon;