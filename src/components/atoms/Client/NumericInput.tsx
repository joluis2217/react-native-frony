import React, { useState } from 'react';
import { TextInput, TextInputProps } from 'react-native';

interface NumericInputProps extends TextInputProps {
  onChangeText: (text: string) => void;
  value: string;
}

const NumericInput: React.FC<NumericInputProps> = ({ value, onChangeText, ...rest }) => {
  const [formattedValue, setFormattedValue] = useState(value);

  const handleTextChange = (text: string) => {
    const numericValue = text.replace(/[^0-9]/g, '');
    onChangeText(numericValue);
    setFormattedValue(numericValue);
  };

  return (
    <TextInput
      {...rest}
      value={formattedValue}
      onChangeText={handleTextChange}
      keyboardType="numeric"
      maxLength={8}
    />
  );
};

export default NumericInput;






