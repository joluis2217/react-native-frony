import React from 'react';
import { Text as RNText } from 'react-native';

const Text: React.FC<{ text: string }> = ({ text }) => {
  return <RNText>{text}</RNText>;
};

export default Text;