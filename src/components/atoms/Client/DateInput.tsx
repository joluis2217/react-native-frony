import React, { useState } from 'react';
import { TextInput } from 'react-native';

const DateInput: React.FC<{ value: string; onChange: (value: string) => void }> = ({ value, onChange }) => {
  const [formattedValue, setFormattedValue] = useState(value);

  const handleTextChange = (text: string) => {
    const numericValue = text.replace(/[^0-9]/g, '');

    if (numericValue.length <= 2) {
      onChange(numericValue);
      setFormattedValue(numericValue);
    } else if (numericValue.length <= 4) {
      onChange(numericValue.slice(0, 2) + '-' + numericValue.slice(2));
      setFormattedValue(numericValue.slice(0, 2) + '-' + numericValue.slice(2));
    } else {
      onChange(numericValue.slice(0, 2) + '-' + numericValue.slice(2, 4) + '-' + numericValue.slice(4, 8));
      setFormattedValue(numericValue.slice(0, 2) + '-' + numericValue.slice(2, 4) + '-' + numericValue.slice(4, 8));
    }
  };

  return (
    <TextInput
      style={{ borderColor: '#ccc', borderWidth: 1, paddingHorizontal: 10, borderRadius: 5, fontSize: 16, height: 40 }}
      value={formattedValue || value}
      onChangeText={handleTextChange}
      placeholder="DD-MM-YYYY"
      maxLength={10}
    />
  );
};

export default DateInput;