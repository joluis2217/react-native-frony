import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

interface ButtonAtomProps {
  onPress: () => void;
}

const ButtonAtom: React.FC<ButtonAtomProps> = ({ onPress }) => {
  return (
    <TouchableOpacity style={styles.button} onPress={onPress}>
      <Text style={styles.buttonText}>+</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#ce0058',
    borderRadius: 30,
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 24,
  },
});

export default ButtonAtom;
