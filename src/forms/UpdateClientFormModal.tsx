import { ClientData } from '../models/Client';
import React, { useState, useEffect } from 'react';
import { Modal, View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { Picker } from '@react-native-picker/picker';

interface ClientFormModalProps {
  isVisible: boolean;
  onClose: () => void;
  onUpdateClient: (clientData: ClientData) => void;
  selectedClient: ClientData | null;
}

const UpdateClientFormModal: React.FC<ClientFormModalProps> = ({ isVisible, onClose, onUpdateClient, selectedClient }) => {
  const [clientData, setClientData] = useState<ClientData>({
    id: 0,
    dni: '',
    name: '',
    lastName: '',
    birthDate: '',
    city: '',
  });

  useEffect(() => {
    if (selectedClient) {
      setClientData(selectedClient);
    } else {
      setClientData({
        id: 0,
        dni: '',
        name: '',
        lastName: '',
        birthDate: '',
        city: '',
      });
    }
  }, [selectedClient]);

  const handleInputChange = (field: keyof ClientData, value: string) => {
    setClientData({ ...clientData, [field]: value });
  };

  const handleUpdateClient = () => {
    onUpdateClient(clientData);
    onClose();
  };

  return (
    <Modal animationType="slide" transparent={true} visible={isVisible}>
      <View style={styles.popupContainer}>
        <View style={styles.popupContent}>
          <Text style={styles.modalTitle}>Editar Cliente</Text>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>DNI:</Text>
            <TextInput
              style={styles.input}
              placeholder="Ingrese el DNI"
              value={clientData.dni}
              maxLength={8}
              editable={false}
              onChangeText={(text) => handleInputChange('dni', text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Nombre:</Text>
            <TextInput
              style={styles.input}
              placeholder="Ingrese el Nombre"
              value={clientData.name}
              onChangeText={(text) => handleInputChange('name', text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Apellidos:</Text>
            <TextInput
              style={styles.input}
              placeholder="Ingrese los Apellidos"
              value={clientData.lastName}
              onChangeText={(text) => handleInputChange('lastName', text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Fecha de Nacimiento:</Text>
            <TextInput
              style={styles.input}
              placeholder="Ingrese la Fecha de Nacimiento"
              value={clientData.birthDate}
              onChangeText={(text) => handleInputChange('birthDate', text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Ciudad:</Text>
            <Picker
              style={styles.picker}
              selectedValue={clientData.city}
              onValueChange={(itemValue) => handleInputChange('city', itemValue)}
            >
              <Picker.Item label="Seleccionar una ciudad" value="" />
              <Picker.Item label="Lima" value="Lima" />
              <Picker.Item label="Callao" value="Callao" />  
            </Picker>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.updateButton} onPress={handleUpdateClient}>
              <Text style={styles.buttonText}>Actualizar</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cancelButton} onPress={onClose}>
              <Text style={styles.buttonText}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  popupContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  popupContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    width: 300,
  },
  modalTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
    color: '#333',
  },
  inputContainer: {
    marginBottom: 15,
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
    color: '#333',
  },
  input: {
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
    fontSize: 16,
    height: 40,
    paddingHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  updateButton: {
    backgroundColor: '#ce0058',
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderRadius: 5,
    flex: 1,
    marginRight: 10,
  },
  cancelButton: {
    backgroundColor: '#ccc',
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderRadius: 5,
    flex: 1,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
  },
  picker: {
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
    height: 40,
    fontSize: 16,
    paddingHorizontal: 10,
  },
});

export default UpdateClientFormModal;
