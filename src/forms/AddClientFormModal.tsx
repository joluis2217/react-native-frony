import { ClientData } from '../models/Client';
import React, { useState } from 'react';
import { Modal, View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import DateInput from '../components/atoms/Client/DateInput';
import NumericInput from '../components/atoms/Client/NumericInput';
interface ClientFormModalProps {
  isVisible: boolean;
  onClose: () => void;
  onAddClient: (clientData: ClientData) => void;
}

const AddClientFormModal: React.FC<ClientFormModalProps> = ({ isVisible, onClose, onAddClient }) => {
  const [clientData, setClientData] = useState<ClientData>({
    id: 0,
    dni: '',
    name: '',
    lastName: '',
    birthDate: '',
    city: '',
  });

 
  const handleInputChange = (field: keyof ClientData, value: string) => {
    setClientData({ ...clientData, [field]: value });
  };

  const handleAddClient = () => {
    onAddClient(clientData);
    setClientData({
      id: 0,
      dni: '',
      name: '',
      lastName: '',
      birthDate: '',
      city: '',
    });
    onClose();
  };

  return (
    <Modal animationType="slide" transparent={true} visible={isVisible}>
      <View style={styles.popupContainer}>
        <View style={styles.popupContent}>
          <Text style={styles.modalTitle}>Agregar Cliente</Text>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>DNI:</Text>
            <NumericInput
          style={styles.input}
          placeholder="Ingrese el DNI"
          value={clientData.dni}
          onChangeText={(text) => handleInputChange('dni', text)}
        />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Nombre:</Text>
            <TextInput
              style={styles.input}
              placeholder="Nombre"
              value={clientData.name}
              onChangeText={(text) => handleInputChange('name', text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Apellidos:</Text>
            <TextInput
              style={styles.input}
              placeholder="Apellidos"
              value={clientData.lastName}
              onChangeText={(text) => handleInputChange('lastName', text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Fecha de Nacimiento:</Text>
            <DateInput
              value={clientData.birthDate}
              onChange={(text) => handleInputChange('birthDate', text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Ciudad:</Text>
            <Picker
              style={styles.input}
              selectedValue={clientData.city}
              onValueChange={(itemValue) => handleInputChange('city', itemValue)}
            >
              <Picker.Item label="Seleccionar una ciudad" value="" />
              <Picker.Item label="Lima" value="Lima" />
              <Picker.Item label="Callao" value="Callao" />
            </Picker>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.addButton} onPress={handleAddClient}>
              <Text style={styles.buttonText}>Agregar</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cancelButton} onPress={onClose}>
              <Text style={styles.buttonText}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  popupContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  popupContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 20,
    width: 300,
  },
  modalTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
    color: '#333',
  },
  inputContainer: {
    marginBottom: 15,
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
  },
  input: {
    borderColor: '#ccc',
    borderWidth: 1,
    paddingHorizontal: 10,
    borderRadius: 5,
    fontSize: 16,
    height: 40,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  addButton: {
    backgroundColor: '#ce0058',
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderRadius: 5,
    flex: 1,
    marginRight: 10,
  },
  cancelButton: {
    backgroundColor: '#ccc',
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderRadius: 5,
    flex: 1,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
  },
});

export default AddClientFormModal;
