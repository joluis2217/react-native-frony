import React, { useCallback,useEffect, useState } from 'react';
import ClientMaintenanceTemplate from '../components/template/ClientMaintenanceTemplate';
import { ClientData } from '../models/Client';
import { updateClientById,createClient,getAllClients,deleteClientById,getClientByDni } from '../services/ServiceClient';
import { calculateAgeDifferenceInYears } from '../utils/utils/validateDates';

const ClientMaintenancePage: React.FC = () => {
  const [clients, setClients] = useState<ClientData[]>([]);
  const [alertContent,setAlertContent] = useState('');
  const [alertContentProp,setAlertContentProp] = useState('');
  const [selectedClient, setSelectedClient] = useState<ClientData | null>(null);
  const [isUpdateFormVisible, setIsUpdateFormVisible] = useState(false);
  const [isAlertVisible, setIsAlertVisible] = useState(false);
  const [isAlertVisibleProp, setIsAlertVisibleProp] = useState(false);
  const [clientProp, setClientProp] = useState<ClientData | null>(null);
  useEffect(() => {
    LoadClient();
  }, []);
    const handleClientPress = useCallback((client: ClientData) => {
    setSelectedClient(client);
    setIsUpdateFormVisible(true);
  }, []);

  const handleClientLongPress = useCallback  (async (client:ClientData) => {
    setAlertContentProp("¿Está seguro de eliminar el cliente?");
    setIsAlertVisibleProp(true);
    setClientProp(client);
  }, []);

    const handleConfirmProp= async ()=>{
        try {
            const newClient = await deleteClientById(clientProp!.id); 
            console.log('Cliente elimnado:', newClient);
            LoadClient();
          } catch (error) {
            console.error('Error al eliminar cliente:', error);
          }
          setIsAlertVisibleProp(false);
    }


    const handleAddClient = async (clientData: ClientData) => {
        const errorMessage = ValidClient(clientData);
        if (errorMessage !== null) {
        setIsAlertVisible(true);
        setAlertContent(errorMessage);
          return;
        }

        try {
          const newClient = await getClientByDni(clientData.dni); 
          if(newClient){
            setIsAlertVisible(true);
            setAlertContent('DNI ya existe');
            return;
          }else{
            try {
              const newClient = await createClient(clientData); 
              setClients((prevClients) => [...prevClients, newClient]);
              setIsAlertVisible(true);
              setAlertContent('Cliente agregado')
              console.log('Cliente agregado:', newClient);
              LoadClient();
            } catch (error) {
                setIsAlertVisible(true);
                setAlertContent('Error al agregar cliente');
              console.error('Error al agregar cliente:', error);
            }
          }
          LoadClient();
        } catch (error) {
        }
  };

    const handleUpdateClient = async (clientData: ClientData) => {
        const errorMessage = ValidClient(clientData);
        if (errorMessage !== null) {
            setIsAlertVisible(true);
            setAlertContent(errorMessage);
          return;
        }
    try {
      const updatedClient = await updateClientById(clientData.id, clientData);
      setIsAlertVisible(true);
      setAlertContent('Cliente Actualizado');
      console.log('Cliente Actualizado:', updatedClient);
      LoadClient();
    } catch (error) {
        setIsAlertVisible(true);
        setAlertContent('Error al actualizar cliente');
      console.error('Error al actualizar cliente:', error);
    }
  };

   const LoadClient=()=>{
    getAllClients()
    .then((clients) => {
      setClients(clients);
    })
    .catch((error) => {
        setIsAlertVisible(true);
        setAlertContent('Error al obtener clientes'+error);
      console.error('Error al obtener clientes:', error);
    });
  }


  function ValidClient(client: ClientData): string | null {
    const birthdate = new Date(client.birthDate);
    console.log(birthdate);
    if (isNaN(birthdate.getTime())) {
      return 'Ingrese un formato de fecha de nacimiento válido.';
    }
    const age = calculateAgeDifferenceInYears(client.birthDate);
    console.log(age);

    function isInvalidDate(date: Date): boolean {
      return isNaN(date.getTime());
    }

    if (isInvalidDate(birthdate))
      return 'Ingrese un formato de fecha de nacimiento válido.';
    if (isMissingField(client.dni))
      return 'Ingresa el DNI.';
    if (isMissingField(client.name))
      return 'Ingresa el Nombre.';
    if (isMissingField(client.lastName))
      return 'Ingresa el apellido.';
    if (isMissingField(client.city))
      return 'Ingresa la ciudad.';
    if (age === null)
    return 'No se puede calcular la edad debido a un problema con la fecha de nacimiento.';

    if (age < 18)
    return 'No se puede registrar, debe ser mayor de 18 años.';

    return null;
  }

  function isMissingField(field: string | undefined): boolean {
    return !field || field.trim() === '';
  }

  return (
    <ClientMaintenanceTemplate
      title="MODULO CLIENTE"
      iconSource={require('../utils/images/client.png')}
      clients={clients}
      onClientPress={handleClientPress}
      onClientLongPress={handleClientLongPress}
      onAddClient={handleAddClient}
      selectedClient={selectedClient}
      isUpdateFormVisible={isUpdateFormVisible}
      setIsUpdateFormVisible={setIsUpdateFormVisible}
      onUpdateClient={handleUpdateClient}
      IsAlertVisible={isAlertVisible}
      SetIsAlertVisible={setIsAlertVisible}
      alertContent={alertContent}
      IsAlertVisibleProp={isAlertVisibleProp}
      SetIsAlertVisibleProp={setIsAlertVisibleProp}
      alertContentProp={alertContentProp}
      onConfirmProp={handleConfirmProp}
      />
  );
};

export default ClientMaintenancePage;
