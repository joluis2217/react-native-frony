import axios from 'axios';
import {ClientData} from '../models/Client';

const BASE_URL = 'http://192.168.1.33:9000/api/client/';

export const getAllClients = async () => {
  try {
    const response = await axios.get(BASE_URL);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const getClientById = async (clientId:number) => {
  try {
    const response = await axios.get(`${BASE_URL}${clientId}`);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const getClientByDni = async (clientDni:string) => {
  try {
    const response = await axios.get(`${BASE_URL}DNI/${clientDni}`);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const createClient = async (newClient:ClientData) => {
  try {
    const response = await axios.post(BASE_URL, newClient);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const updateClientById = async (clientId:number, updatedClient:ClientData) => {
  try {
    const response = await axios.put(`${BASE_URL}${clientId}`, updatedClient);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const deleteClientById = async (clientId:number) => {
  try {
    const response = await axios.delete(`${BASE_URL}${clientId}`);
    return response.data;
  } catch (error) {
    throw error;
  }
};
