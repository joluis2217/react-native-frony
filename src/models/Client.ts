export class ClientData {
    constructor(
      public id: number,
      public dni: string,
      public name: string,
      public lastName: string,
      public birthDate: string,
      public city: string
    ) {}
  }