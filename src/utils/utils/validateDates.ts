export const calculateAgeDifferenceInYears = (dateString: string): number | null => {
  const dateParts = dateString.split('-');
  if (dateParts.length !== 3) {
    return null;
  }

  const day = parseInt(dateParts[0]);
  const month = parseInt(dateParts[1]);
  const year = parseInt(dateParts[2]);
  if (isNaN(day) || isNaN(month) || isNaN(year)) {
    return null;
  }
  const birthDate = new Date(year, month - 1, day);

  if (isNaN(birthDate.getTime())) {
    return null;
  }

  const currentDate = new Date();
  const differenceInMilliseconds = currentDate.getTime() - birthDate.getTime();
  const ageDifferenceInYears = Math.floor(differenceInMilliseconds / (1000 * 60 * 60 * 24 * 365.25));
  return ageDifferenceInYears;
}